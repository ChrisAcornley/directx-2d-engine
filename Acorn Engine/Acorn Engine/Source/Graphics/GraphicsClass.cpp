#include "GraphicsClass.hpp"

GraphicsClass::GraphicsClass(void)
{
	m_DirectXController = NULL;
	m_TextureShader = NULL;
	m_ColourShader = NULL;
	m_Renderer2D = NULL;
	m_GameCamera = NULL;
	m_Bitmap = NULL;
	m_Line = NULL;
}

GraphicsClass::~GraphicsClass(void)
{
}

bool GraphicsClass::Initialise(HWND p_WindowHandle, int p_ScreenResolutionWidth, int p_ScreenResolutionHeight)
{
	m_ScreenResolutionWidth = p_ScreenResolutionWidth;
	m_ScreenResolutionHeight = p_ScreenResolutionHeight;

	m_DirectXController = new DirectXClass();
	if(!m_DirectXController) {
		MessageBox(p_WindowHandle, L"The DirectX class failed to initialise.", L"Error - GraphicsClass::Initialise()", MB_OK | MB_ICONWARNING);
		return false;
	}

	if(!m_DirectXController->InitialiseDirectX(p_WindowHandle, p_ScreenResolutionWidth, p_ScreenResolutionHeight)) {
		MessageBox(p_WindowHandle, L"The DirectX class failed its initialise call.", L"Error - GraphicsClass::Initialise()", MB_OK | MB_ICONWARNING);
		return false;
	}

	m_TextureShader = new TextureShaderClass();
	if(!m_TextureShader) {
		MessageBox(p_WindowHandle, L"The texture class failed to create.", L"Error - GraphicsClass::Initialise()", MB_OK | MB_ICONWARNING);
		return false;
	}

	if(!m_TextureShader->Initialize(m_DirectXController->GetDirectXDevice(), p_WindowHandle, L"Resources/Shaders/texture.fx")) {
		MessageBox(p_WindowHandle, L"The texture class failed to initialise.", L"Error - GraphicsClass::Initialise()", MB_OK | MB_ICONWARNING);
		return false;
	}

	m_ColourShader = new ColourShaderClass();
	if(!m_ColourShader) {
		MessageBox(p_WindowHandle, L"The shader class failed to create.", L"Error - GraphicsClass::Initialise()", MB_OK | MB_ICONWARNING);
		return false;
	}

	if(!m_ColourShader->Initialize(m_DirectXController->GetDirectXDevice(), p_WindowHandle, L"Resources/Shaders/colour.fx")) {
		MessageBox(p_WindowHandle, L"The shader class failed to initialise.", L"Error - GraphicsClass::Initialise()", MB_OK | MB_ICONWARNING);
		return false;
	}

	m_GameCamera = new CameraClass();
	if(!m_GameCamera) {
		MessageBox(p_WindowHandle, L"The camera class failed to create.", L"Error - GraphicsClass::Initialise()", MB_OK | MB_ICONWARNING);
		return false;
	}

	m_GameCamera->SetPosition(0.0f, 0.0f, -1.0f);
	m_GameCamera->Render();

	return true;
}

Renderer2D* GraphicsClass::Create2DRenderer()
{
	m_Renderer2D = NULL;

	m_Renderer2D = new Renderer2D();
	if(!m_Renderer2D) {
		MessageBox(NULL, L"Could not create a 2D Renderer.", L"Error - GraphicsClass::Initialise()", MB_OK | MB_ICONWARNING);
		return NULL;
	}

	D3DXMATRIX l_WorldMatrix;
	m_DirectXController->GetWorldMatrix(l_WorldMatrix);

	D3DXMATRIX l_ProjectionMatrix;
	m_DirectXController->GetProjectionMatrix(l_ProjectionMatrix);

	D3DXMATRIX l_OrthographicMatrix;
	m_DirectXController->GetOrthographicMatrix(l_OrthographicMatrix);
	
	D3DXMATRIX l_ViewMatrix;
	m_GameCamera->GetViewMatrix(l_ViewMatrix);

	if(!m_Renderer2D->Initialise(m_DirectXController->GetDirectXDeviceContext(), l_WorldMatrix, l_OrthographicMatrix, l_ProjectionMatrix, l_ViewMatrix, m_TextureShader, m_ColourShader)) {
		MessageBox(NULL, L"Could not initilaise a 2D Renderer.", L"Error - GraphicsClass::Create2DRenderer()", MB_OK | MB_ICONWARNING);
		return NULL;
	}

	return m_Renderer2D;
}

void GraphicsClass::Close2DRenderer(Renderer2D* p_Renderer2D)
{
	m_Renderer2D = NULL;
	p_Renderer2D->Shutdown();
	delete p_Renderer2D;
	p_Renderer2D = NULL;
}

BitmapClass* GraphicsClass::CreateBitmap(int p_BitmapHeight, int p_BitmapWidth)
{
	m_Bitmap = NULL;

	m_Bitmap = new BitmapClass();
	if(!m_Bitmap) {
		return NULL;
	}

	if(!m_Bitmap->Initialize(	m_DirectXController->GetDirectXDevice(),
								m_DirectXController->GetDirectXDeviceContext(),
								m_ScreenResolutionWidth,
								m_ScreenResolutionHeight,
								p_BitmapHeight,
								p_BitmapWidth)) {
		return NULL;
	}

	return m_Bitmap;
}

void GraphicsClass::CloseBitmap(BitmapClass* p_Bitmap)
{
	m_Bitmap = NULL;
	p_Bitmap->Shutdown();
	delete p_Bitmap;
	p_Bitmap = NULL;
}

LineClass* GraphicsClass::CreateLine()
{
	m_Line = NULL;

	m_Line = new LineClass();
	if(!m_Line) {
		return false;
	}

	if(!m_Line->Initialize(	m_DirectXController->GetDirectXDevice(),
								m_DirectXController->GetDirectXDeviceContext(),
								m_ScreenResolutionWidth,
								m_ScreenResolutionHeight)) {
		return NULL;
	}

	return m_Line;
}

void GraphicsClass::CloseLine(LineClass* p_LineClass)
{
	m_Line = NULL;
	p_LineClass->Shutdown();
	delete p_LineClass;
	p_LineClass = NULL;
}

void GraphicsClass::Shutdown()
{
	if(m_ColourShader) {
		m_ColourShader->Shutdown();
		delete m_ColourShader;
		m_ColourShader = NULL;
	}

	if(m_TextureShader) {
		m_TextureShader->Shutdown();
		delete m_TextureShader;
		m_TextureShader = NULL;
	}

	if(m_DirectXController) {
		m_DirectXController->ShutdownDirectX();
		delete m_DirectXController;
		m_DirectXController = NULL;
	}

	if(m_GameCamera) {
		delete m_GameCamera;
		m_GameCamera = NULL;
	}

	return;
}

void GraphicsClass::BeginScene(float r, float g, float b, float a)
{
	m_DirectXController->BeginScene(r, g, b, a);
	m_DirectXController->TurnZBufferOff();
}

void GraphicsClass::EndScene()
{
	m_DirectXController->TurnZBufferOn();
	m_DirectXController->EndScene();
}
