#include "DirectXClass.hpp"


DirectXClass::DirectXClass(void)
{
	m_DXDevice = NULL;
	m_DXDeviceContext = NULL;
	m_DXSwapChain = NULL;
	m_DXRenderTargetView = NULL;
	m_DXDepthStencil = NULL;
	m_DXDepthStencilView = NULL;
	m_depthStencilState = NULL;
	m_depthDisabledStencilState = NULL;
	m_alphaEnableBlendingState = NULL;
	m_alphaDisableBlendingState = NULL;
}


DirectXClass::~DirectXClass(void)
{
	// Nothing to clean up
}

bool DirectXClass::InitialiseDirectX(HWND windowHandle, int screenRezX, int screenRezY)
{
	D3D_DRIVER_TYPE         g_driverType = D3D_DRIVER_TYPE_NULL;
	D3D_FEATURE_LEVEL       g_featureLevel = D3D_FEATURE_LEVEL_10_0;

	const float screenDepth = 1000.0f;
	const float screenNear = 0.1f;

	float fieldOfView, screenAspect;

	// Create an HR result and set it to initialially be true
	HRESULT hr = S_OK;

	// Set the width and height of the game screen
	UINT width = screenRezX;
	UINT height = screenRezY;

	UINT createDeviceFlags = 0;

	D3D_DRIVER_TYPE driverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    UINT numDriverTypes = ARRAYSIZE( driverTypes );

    D3D_FEATURE_LEVEL featureLevels[] =
    {
		D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
    };
	UINT numFeatureLevels = ARRAYSIZE( featureLevels );

	D3D11_DEPTH_STENCIL_DESC depthDisabledStencilDesc;

	DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory( &sd, sizeof(sd) );
    sd.BufferCount = 1;
    sd.BufferDesc.Width = width;
    sd.BufferDesc.Height = height;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = windowHandle;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;
	
	for( UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++ )
    {
        g_driverType = driverTypes[driverTypeIndex];
        hr = D3D11CreateDeviceAndSwapChain( NULL, g_driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
                                            D3D11_SDK_VERSION, &sd, &m_DXSwapChain, &m_DXDevice, &g_featureLevel, &m_DXDeviceContext );
        if( SUCCEEDED( hr ) )
            break;
    }
    if( FAILED( hr ) )
        return false;

	 // Create a render target view
    ID3D11Texture2D *pBackBuffer;
    if( FAILED( m_DXSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), (LPVOID*)&pBackBuffer ) ) )
        return FALSE;

    hr = m_DXDevice->CreateRenderTargetView( pBackBuffer, NULL, &m_DXRenderTargetView );
    pBackBuffer->Release();
    if( FAILED( hr ) )
        return FALSE;

	// Create depth stencil texture
    D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory( &descDepth, sizeof(descDepth) );
    descDepth.Width = width;
    descDepth.Height = height;
    descDepth.MipLevels = 1;
    descDepth.ArraySize = 1;
    descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    descDepth.SampleDesc.Count = 1;
    descDepth.SampleDesc.Quality = 0;
    descDepth.Usage = D3D11_USAGE_DEFAULT;
    descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    descDepth.CPUAccessFlags = 0;
    descDepth.MiscFlags = 0;
    hr = m_DXDevice->CreateTexture2D( &descDepth, NULL, &m_DXDepthStencil );
    if( FAILED( hr ) )
        return false;

	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	// Initialize the description of the stencil state.
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	// Set up the description of the stencil state.
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing.
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing.
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create the depth stencil state.
	hr = m_DXDevice->CreateDepthStencilState(&depthStencilDesc, &m_depthStencilState);
	if(FAILED(hr))
	{
		return false;
	}

	// Set the depth stencil state.
	m_DXDeviceContext->OMSetDepthStencilState(m_depthStencilState, 1);

    // Create the depth stencil view
    D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory( &descDSV, sizeof(descDSV) );
    descDSV.Format = descDepth.Format;
    descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    descDSV.Texture2D.MipSlice = 0;
    hr = m_DXDevice->CreateDepthStencilView( m_DXDepthStencil, &descDSV, &m_DXDepthStencilView );
    if( FAILED( hr ) )
        return false;

    m_DXDeviceContext->OMSetRenderTargets( 1, &m_DXRenderTargetView, m_DXDepthStencilView );

	D3D11_RASTERIZER_DESC rasterDesc;
	// Setup the raster description which will determine how and what polygons will be drawn.
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state from the description we just filled out.
	hr = m_DXDevice->CreateRasterizerState(&rasterDesc, &m_DXRasterState);
	if(FAILED(hr))
	{
		return false;
	}

	// Now set the rasterizer state.
	m_DXDeviceContext->RSSetState(m_DXRasterState);

	// Set the viewport
	D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)width;
    vp.Height = (FLOAT)height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
    m_DXDeviceContext->RSSetViewports( 1, &vp );

	// Setup the projection matrix.
	fieldOfView = (float)D3DX_PI / 4.0f;
	screenAspect = (float)width / (float)height;

	// Create the projection matrix for 3D rendering.
	D3DXMatrixPerspectiveFovLH(&m_Projection, fieldOfView, screenAspect, screenNear, screenDepth);

	// Initialize the world matrix to the identity matrix.
	D3DXMatrixIdentity(&m_World);

	// Create an orthographic projection matrix for 2D rendering.
	D3DXMatrixOrthoLH(&m_Orthographic, (float)width, (float)height, screenNear, screenDepth);

	// Clear the second depth stencil state before setting the parameters.
	ZeroMemory(&depthDisabledStencilDesc, sizeof(depthDisabledStencilDesc));

	// Now create a second depth stencil state which turns off the Z buffer for 2D rendering.  The only difference is 
	// that DepthEnable is set to false, all other parameters are the same as the other depth stencil state.
	depthDisabledStencilDesc.DepthEnable = false;
	depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDisabledStencilDesc.StencilEnable = true;
	depthDisabledStencilDesc.StencilReadMask = 0xFF;
	depthDisabledStencilDesc.StencilWriteMask = 0xFF;
	depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create the state using the device.
	hr = m_DXDevice->CreateDepthStencilState(&depthDisabledStencilDesc, &m_depthDisabledStencilState);
	if(FAILED(hr))
	{
		return false;
	}

	D3D11_BLEND_DESC		blendStateDescription;

	// Clear the blend state description.
	ZeroMemory(&blendStateDescription, sizeof(D3D11_BLEND_DESC));

	// Create an alpha enabled blend state description.
	blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
	blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendStateDescription.RenderTarget[0].RenderTargetWriteMask = 0x0f;

	// Create the blend state using the description.
	hr = m_DXDevice->CreateBlendState(&blendStateDescription, &m_alphaEnableBlendingState);
	if(FAILED(hr))
	{
		return false;
	}

	// Modify the description to create an alpha disabled blend state description.
	blendStateDescription.RenderTarget[0].BlendEnable = FALSE;

	// Create the blend state using the description.
	hr = m_DXDevice->CreateBlendState(&blendStateDescription, &m_alphaDisableBlendingState);
	if(FAILED(hr))
	{
		return false;
	}

	return true;

}

/*
HRESULT DirectXClass::CompileShaderFromFile( WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut )
{
    HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = D3DX11CompileFromFile( szFileName, NULL, NULL, szEntryPoint, szShaderModel, 
        dwShaderFlags, 0, NULL, ppBlobOut, &pErrorBlob, NULL );
    if( FAILED(hr) )
    {
        if( pErrorBlob != NULL )
            OutputDebugStringA( (char*)pErrorBlob->GetBufferPointer() );
        if( pErrorBlob ) pErrorBlob->Release();
        return hr;
    }
    if( pErrorBlob ) pErrorBlob->Release();

    return S_OK;
}
*/

void DirectXClass::ShutdownDirectX() 
{
	if(m_alphaEnableBlendingState)
	{
		m_alphaEnableBlendingState->Release();
		m_alphaEnableBlendingState = 0;
	}

	if(m_alphaDisableBlendingState)
	{
		m_alphaDisableBlendingState->Release();
		m_alphaDisableBlendingState = 0;
	}

	// Clear the device context state - Reset the context to default values
	if( m_DXDeviceContext ) {
		m_DXDeviceContext->ClearState();
	}

	// Release the depth stencil state and nullify the pointer
	if(m_depthDisabledStencilState) {
		m_depthDisabledStencilState->Release();
		m_depthDisabledStencilState = 0;
	}

	if(m_depthStencilState) {
		m_depthStencilState->Release();
		m_depthStencilState = 0;
	}

	// Release the depth stencil object and nullify the pointer
	if( m_DXDepthStencil ) {
		m_DXDepthStencil->Release();
		m_DXDepthStencil = NULL;
	}
	
	// Release the depth stencil view object and nullify the pointer
	if( m_DXDepthStencilView ) {
		m_DXDepthStencilView->Release();
		m_DXDepthStencilView = NULL;
	}
	
	// Release the Raster state object and nullify the pointer
	if (m_DXRasterState ) {
		m_DXRasterState->Release();
		m_DXRasterState = NULL;
	}

	// Release the rendering target and nullify the pointer
    if( m_DXRenderTargetView ) {
		m_DXRenderTargetView->Release();
		m_DXRenderTargetView = NULL;
	}

	// Release the Swap Chain object and nullify the pointer
    if( m_DXSwapChain ) {
		m_DXSwapChain->Release();
		m_DXSwapChain = NULL;
	}

	// Release the Device Context object and nullify the pointer
    if( m_DXDeviceContext ) {
		m_DXDeviceContext->Release();
		m_DXDeviceContext = NULL;
	}

	// Release Device object and nullify pointer
    if( m_DXDevice ) {
		m_DXDevice->Release();
		m_DXDevice = NULL;
	}

	return;
}

void DirectXClass::BeginScene(float red, float green, float blue, float alpha)
{
	float color[4];


	// Setup the color to clear the buffer to.
	color[0] = red;
	color[1] = green;
	color[2] = blue;
	color[3] = alpha;

	// Clear the back buffer.
	m_DXDeviceContext->ClearRenderTargetView(m_DXRenderTargetView, color);
    
	// Clear the depth buffer.
	m_DXDeviceContext->ClearDepthStencilView(m_DXDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	return;
}


void DirectXClass::EndScene()
{
	bool m_vsync_enabled = true;

	// Present the back buffer to the screen since rendering is complete.
	if(m_vsync_enabled)
	{
		// Lock to screen refresh rate.
		m_DXSwapChain->Present(1, 0);
	}
	else
	{
		// Present as fast as possible.
		m_DXSwapChain->Present(0, 0);
	}

	return;
}

void DirectXClass::TurnZBufferOn()
{
	m_DXDeviceContext->OMSetDepthStencilState(m_depthStencilState, 1);
	return;
}


void DirectXClass::TurnZBufferOff()
{
	m_DXDeviceContext->OMSetDepthStencilState(m_depthDisabledStencilState, 1);
	return;
}

ID3D11DepthStencilView* DirectXClass::GetDepthStencilView()
{
	return m_DXDepthStencilView;
}

void DirectXClass::SetBackBufferRenderTarget()
{
	// Bind the render target view and depth stencil buffer to the output render pipeline.
	m_DXDeviceContext->OMSetRenderTargets(1, &m_DXRenderTargetView, m_DXDepthStencilView);

	return;
}

void DirectXClass::TurnOnAlphaBlending()
{
	float blendFactor[4];
	

	// Setup the blend factor.
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;
	
	// Turn on the alpha blending.
	m_DXDeviceContext->OMSetBlendState(m_alphaEnableBlendingState, blendFactor, 0xffffffff);

	return;
}

void DirectXClass::TurnOffAlphaBlending()
{
	float blendFactor[4];
	

	// Setup the blend factor.
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;
	
	// Turn off the alpha blending.
	m_DXDeviceContext->OMSetBlendState(m_alphaDisableBlendingState, blendFactor, 0xffffffff);

	return;
}