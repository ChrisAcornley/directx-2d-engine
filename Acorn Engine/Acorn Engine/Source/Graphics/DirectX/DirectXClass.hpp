// 02/02/2014

#ifndef _DIRECT_X_CLASS_HPP_
#define _DIRECT_X_CLASS_HPP_

#include <windows.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dcompiler.h>
#include <D3DX10math.h>
#include <xnamath.h>

class DirectXClass
{
public:

	DirectXClass(void);

	~DirectXClass(void);

	bool InitialiseDirectX(HWND windowHandle, int screenRezX, int screenRezY);

	ID3D11Device* GetDirectXDevice() { return m_DXDevice; }

	ID3D11DeviceContext* GetDirectXDeviceContext() { return m_DXDeviceContext; }

	void ShutdownDirectX();

	void BeginScene(float, float, float, float);

	void EndScene();

	void TurnZBufferOn();

	void TurnZBufferOff();

	ID3D11DepthStencilView* GetDepthStencilView();

	void SetBackBufferRenderTarget();

	void TurnOnAlphaBlending();

	void TurnOffAlphaBlending();

	void GetWorldMatrix(D3DXMATRIX& p_WorldMatrix) { p_WorldMatrix = m_World; }

	void GetProjectionMatrix(D3DXMATRIX& p_ProjectionMatrix) { p_ProjectionMatrix = m_Projection; }

	void GetOrthographicMatrix(D3DXMATRIX& p_OrthographicMatrix) {p_OrthographicMatrix = m_Orthographic; }

private:

	ID3D11Device*				m_DXDevice;
	ID3D11DeviceContext*		m_DXDeviceContext;
	IDXGISwapChain*				m_DXSwapChain;
	ID3D11RenderTargetView*		m_DXRenderTargetView;
	ID3D11Texture2D*			m_DXDepthStencil;
	ID3D11DepthStencilView*		m_DXDepthStencilView;
	ID3D11RasterizerState*	    m_DXRasterState;
	ID3D11DepthStencilState*	m_depthStencilState;
	ID3D11DepthStencilState*	m_depthDisabledStencilState;
	ID3D11BlendState*			m_alphaEnableBlendingState;
	ID3D11BlendState*			m_alphaDisableBlendingState;
	D3DXMATRIX					m_World;
	D3DXMATRIX					m_Projection;
	D3DXMATRIX					m_Orthographic;
};

#endif

