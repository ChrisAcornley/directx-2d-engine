// 03/02/2014

#ifndef _BITMAP_CLASS_HPP_
#define _BITMAP_CLASS_HPP_

//////////////
// INCLUDES //
//////////////
#include <d3d11.h>
#include <d3dx10math.h>
#include <d3dx11tex.h>

class BitmapClass
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};

public:

	BitmapClass(void);
	~BitmapClass(void);

	bool Initialize(ID3D11Device*, ID3D11DeviceContext*, int screenWidth, int screenHeight, int bitmapWidth, int bitmapHeight);
	void Shutdown();

	int GetIndexCount();
	ID3D11ShaderResourceView* GetTexture();
	bool Update(int, int);
	void ChangeSize(int p_NewWidth, int p_NewHeight);
	bool LoadTexture(WCHAR*);

	int GetBitmapWidth() { return m_bitmapWidth; }
	int GetBitmapHeight() { return m_bitmapHeight; }

	ID3D11Buffer* GetVertexBuffer() { return m_vertexBuffer; }
	ID3D11Buffer* GetIndexBuffer() { return m_indexBuffer; }

private:
	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	void ReleaseTexture();

private:

	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;
	int m_bitmapWidth, m_bitmapHeight;
	int m_previousPosX, m_previousPosY;
	int m_screenWidth, m_screenHeight;
	ID3D11Device *m_D3DDevice;
	ID3D11DeviceContext	*m_D3DDeviceContext;
	ID3D11ShaderResourceView *m_Texture;
};

#endif
