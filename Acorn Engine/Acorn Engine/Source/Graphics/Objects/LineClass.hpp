// 10/02/2014

// This class has more a complicated initialisation

#ifndef _LINE_CLASS_HPP_
#define _LINE_CLASS_HPP_

#include <D3DX11.h>
#include <D3DX10math.h>

// This is a struct that contains the vector locations of the control points of a Bezier curve
// there can be up to six points on the curve but not all of them need to have data
// The integer called m_NumberOfLinePoints tells the class how many vector co-ordinates are needed in the drawing
// The integer called m_NumberOfControlPoints tells the class how many control points are initialised.
// Giving an incorrect number could yield some strange results.
struct CONTROL_DATA {
	D3DXVECTOR3 m_ControlPoints[6];
	unsigned int m_NumberOfLinePoints;
	unsigned int m_NumberOfControlPoints;
};

// This is a struct that allows the application to pass in several sets of control points that draw
// independently of each other. This allows curved lines, then a straight (using two control points)
// then a curve again.
// The integer called m_NumberOfControlDataSets tells the class how many sets of CONTROL_DATA structs
// are and therefore how many seperate lines there are all together
struct LINE_DATA {
	CONTROL_DATA m_ControlData[6];
	int m_NumberOfControlDataSets;
};

struct DE_CASTELJAU_COORD {
	float x, y;
};

class LineClass
{
private:
	struct LineVertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR4 color;
	};

public:
	LineClass(void);
	~LineClass(void);

	bool Initialize(ID3D11Device* device, ID3D11DeviceContext* deviceContext, int screenWidth, int screenHeight);
	void Shutdown();
	int GetIndexCount();

	ID3D11Buffer* GetVertexBuffer() { return m_vertexBuffer; }
	ID3D11Buffer* GetIndexBuffer() { return m_indexBuffer; }

private:
	bool InitializeBuffers();
	bool InitializeBuffers(const CONTROL_DATA);
	bool InitializeBuffers(const LINE_DATA);
	void ShutdownBuffers();

	DE_CASTELJAU_COORD GetDeCasteljauValue(const float p_Time, float *p_XValues, float *p_YValues, const int p_NumberOfControlPoints);

private:

	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;
	int m_screenWidth, m_screenHeight;
	ID3D11Device *m_D3DDevice;
	ID3D11DeviceContext	*m_D3DDeviceContext;
};

#endif

