#include "LineClass.hpp"

LineClass::LineClass(void)
{
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	m_D3DDevice = 0;
	m_D3DDeviceContext = 0;
}

LineClass::~LineClass(void)
{
}

bool LineClass::Initialize(ID3D11Device* device, ID3D11DeviceContext* deviceContext, int screenWidth, int screenHeight)
{
	bool result;

	m_D3DDevice = device;
	m_D3DDeviceContext = deviceContext;

	// Store the screen size.
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	CONTROL_DATA temp;
	temp.m_NumberOfControlPoints = 4;
	temp.m_NumberOfLinePoints = 200;
	temp.m_ControlPoints[0] = D3DXVECTOR3(-100.0f, 0.0f, 0.0f);
	temp.m_ControlPoints[3] = D3DXVECTOR3(0.0f, 20.0f, 0.0f);
	temp.m_ControlPoints[1] = D3DXVECTOR3(100.0f, 0.0f, 0.0f);
	temp.m_ControlPoints[2] = D3DXVECTOR3(150.0f, 150.0f, 0.0f);

	result = InitializeBuffers(temp);
	if(!result) 
	{
		return false;
	}

	return true;
}

void LineClass::Shutdown()
{
	// Shutdown the vertex and index buffers.
	ShutdownBuffers();

	if(m_D3DDeviceContext) {
		m_D3DDeviceContext = 0;
	}

	if(m_D3DDevice) {
		m_D3DDevice = 0;
	}

	return;
}

int LineClass::GetIndexCount()
{
	return m_indexCount;
}

bool LineClass::InitializeBuffers()
{
	LineVertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;

	// Set the number of vertices in the vertex array.
	m_vertexCount = 3;

	// Set the number of indices in the index array.
	m_indexCount = 3;

	// Create the vertex array.
	vertices = new LineVertexType[m_vertexCount];
	if(!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices)
	{
		return false;
	}

	// Load the vertex array with data.
	vertices[0].position = D3DXVECTOR3(-40.0f, 0.0f, 0.0f);  // Bottom left.
	vertices[0].color = D3DXVECTOR4(0.0f, 1.0f, 0.0f, 1.0f);

	vertices[1].position = D3DXVECTOR3(0.0f, 40.0f, 0.0f);  // Top middle.
	vertices[1].color = D3DXVECTOR4(0.0f, 1.0f, 0.0f, 1.0f);

	vertices[2].position = D3DXVECTOR3(40.0f, 0.0f, 0.0f);  // Bottom right.
	vertices[2].color = D3DXVECTOR4(0.0f, 1.0f, 0.0f, 1.0f);

	// Load the index array with data.
	indices[0] = 0;  // Bottom left.
	indices[1] = 1;  // Top middle.
	indices[2] = 2;  // Bottom right.

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(LineVertexType) * m_vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	result = m_D3DDevice->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = m_D3DDevice->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	return true;
}

bool LineClass::InitializeBuffers(const CONTROL_DATA p_ControlData)
{
	LineVertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	const int numberOfControlPoints = p_ControlData.m_NumberOfControlPoints;

	// Set the number of vertices in the vertex array.
	m_vertexCount = p_ControlData.m_NumberOfLinePoints;

	// Set the number of indices in the index array.
	m_indexCount = p_ControlData.m_NumberOfLinePoints;

	// Create the vertex array.
	vertices = new LineVertexType[m_vertexCount];
	if(!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices)
	{
		return false;
	}

	float* l_XValues;
	l_XValues = new float[p_ControlData.m_NumberOfControlPoints];
	if(!l_XValues) {
		return false;
	}

	float* l_YValues;
	l_YValues = new float[p_ControlData.m_NumberOfControlPoints];
	if(!l_YValues) {
		return false;
	}

	for(int i = 0; i < p_ControlData.m_NumberOfControlPoints; i++) {
		l_XValues[i] = p_ControlData.m_ControlPoints[i].x;
		l_YValues[i] = p_ControlData.m_ControlPoints[i].y;
	}

	DE_CASTELJAU_COORD temp;

	for(int i = 0; i < p_ControlData.m_NumberOfLinePoints; i++) {
		float time = ((float)i /  (float)(p_ControlData.m_NumberOfLinePoints - 1));

		temp = GetDeCasteljauValue(time, l_XValues, l_YValues, p_ControlData.m_NumberOfControlPoints);
		vertices[i].position = D3DXVECTOR3(temp.x, temp.y, 0.0f);
	}

	// Set all vertices to have the colour green 
	for(int i = 0; i < m_vertexCount; i++) {
		vertices[i].color = D3DXVECTOR4(0.0f, 1.0f, 0.0f, 1.0f);
	}

	// Load the index array with data.
	for(int i = 0; i < m_indexCount; i++) {
		indices[i] = i;
	}

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(LineVertexType) * m_vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	result = m_D3DDevice->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = m_D3DDevice->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	delete [] l_XValues;
	l_XValues = 0;

	delete [] l_YValues;
	l_YValues = 0;

	return true;
}

bool LineClass::InitializeBuffers(const LINE_DATA p_LineData)
{
	LineVertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;

	// Set the number of vertices in the vertex array.
	m_vertexCount = 3;

	// Set the number of indices in the index array.
	m_indexCount = 3;

	// Create the vertex array.
	vertices = new LineVertexType[m_vertexCount];
	if(!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices)
	{
		return false;
	}

	// Load the vertex array with data.

	// Load the index array with data.

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(LineVertexType) * m_vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	result = m_D3DDevice->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = m_D3DDevice->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	return true;
}

DE_CASTELJAU_COORD LineClass::GetDeCasteljauValue(const float p_Time, float *p_XValues, float *p_YValues, const int p_NumberOfControlPoints)
{
	DE_CASTELJAU_COORD temp;

	float l_XValues[15];
	float l_YValues[15];

	int decrementNumber = 1;
	int store = 0;
	bool check = true;
	int loopLineLength = p_NumberOfControlPoints - 1;
	int loopIncrement = 0;
	int adder = 0;

	while(check) {
		if((p_NumberOfControlPoints - decrementNumber) == 0) {
			check = false;
		}
		else {
			store += p_NumberOfControlPoints - decrementNumber;
			decrementNumber++;
		}
	}

	store += p_NumberOfControlPoints;

	// Pass in all the control points
	for(int i = 0; i < p_NumberOfControlPoints; i++) {
		l_XValues[i] = p_XValues[i];
		l_YValues[i] = p_YValues[i];
	}

	for(int i = p_NumberOfControlPoints; i < store; i++) {
		// goes through each control points
		l_XValues[i] = ((1.0f - p_Time)*(l_XValues[i-p_NumberOfControlPoints+adder])) + ((p_Time)*(l_XValues[i - (p_NumberOfControlPoints-1) + adder]));
		l_YValues[i] = ((1.0f - p_Time)*(l_YValues[i-p_NumberOfControlPoints+adder])) + ((p_Time)*(l_YValues[i - (p_NumberOfControlPoints-1) + adder]));
		loopIncrement++;
		if(loopIncrement == loopLineLength) {
			loopLineLength --;
			loopIncrement = 0;
			adder++;
		}
	}

	

	// Get the last x and y values and pass them into the struct to return
	/////////////////////
	temp.x = l_XValues[store - 1];
	temp.y = l_YValues[store - 1];
	
	////////////////////

	p_XValues = 0;
	p_YValues = 0;

	return temp;
}

void LineClass::ShutdownBuffers()
{
	// Release the index buffer.
	if(m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}
