// 03/02/2014

#ifndef _CAMERA_CLASS_HPP_
#define _CAMERA_CLASS_HPP_

//////////////
// INCLUDES //
//////////////
#include <d3dx10math.h>

class CameraClass
{
public:
	CameraClass(void);
	~CameraClass(void);

	void SetPosition(float, float, float);
	void SetRotation(float, float, float);

	D3DXVECTOR3 GetPosition() { return D3DXVECTOR3(m_positionX, m_positionY, m_positionZ); }
	D3DXVECTOR3 GetRotation() { return D3DXVECTOR3(m_rotationX, m_rotationY, m_rotationZ); }

	void Render();
	void GetViewMatrix(D3DXMATRIX& passMatrix) { passMatrix = m_viewMatrix; }

private:
	float m_positionX, m_positionY, m_positionZ;
	float m_rotationX, m_rotationY, m_rotationZ;
	D3DXMATRIX m_viewMatrix;
};

#endif

