#include "Renderer2D.hpp"


Renderer2D::Renderer2D(void)
{
	m_TextureShader = NULL;
	m_ColourShader = NULL;
}


Renderer2D::~Renderer2D(void)
{
}

bool Renderer2D::Initialise(ID3D11DeviceContext* p_DirectXDeviceContext, D3DXMATRIX& p_WorldMatrix, D3DXMATRIX& p_OrthographicMatrix, D3DXMATRIX& p_ProjectionMatrix, D3DXMATRIX& p_ViewMatrix, TextureShaderClass* p_TextureShader, ColourShaderClass* p_ColourShader)
{
	m_DirectXDeviceContext = p_DirectXDeviceContext;
	if(!m_DirectXDeviceContext) {
		MessageBox(NULL, L"The DirectX Device Context did not correctly move to the 2D Renderer.", L"Error - Renderer2D::Initialise()", MB_OK | MB_ICONWARNING); 	
		return false;
	}

	m_TextureShader = p_TextureShader;
	if(!m_TextureShader) {
		MessageBox(NULL, L"The Texture Shader did not correctly move to the 2D Renderer.", L"Error - Renderer2D::Initialise()", MB_OK | MB_ICONWARNING); 	
		return false;
	}

	m_ColourShader = p_ColourShader;
	if(!m_ColourShader) {
		MessageBox(NULL, L"The Colour Shader did not correctly move to the 2D Renderer.", L"Error - Renderer2D::Initialise()", MB_OK | MB_ICONWARNING); 	
		return false;
	}

	m_WorldMatrix = p_WorldMatrix;
	m_OrthographicMatrix = p_OrthographicMatrix;
	m_ProjectionMatrix = p_ProjectionMatrix;
	m_ViewMatrix = p_ViewMatrix;

	return true;
}

bool Renderer2D::Render(BitmapClass* p_RenderBitmap)
{
	// Render Model
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType); 

	// Set the drawing offset to zero (start drawing from the beginning)
	offset = 0;

	ID3D11Buffer *l_VertexBuffer = p_RenderBitmap->GetVertexBuffer();
	ID3D11Buffer *l_IndexBuffer = p_RenderBitmap->GetIndexBuffer();
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	m_DirectXDeviceContext->IASetVertexBuffers(0, 1, &l_VertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	m_DirectXDeviceContext->IASetIndexBuffer(l_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	m_DirectXDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Render Shader
	if(!m_TextureShader->Render(m_DirectXDeviceContext, p_RenderBitmap->GetIndexCount(), m_WorldMatrix, m_ViewMatrix, m_OrthographicMatrix, p_RenderBitmap->GetTexture())) {
		MessageBox(NULL, L"The Texture Shader did not render correctly.", L"Error - Renderer2D::Render(BitmapClass*)", MB_OK | MB_ICONWARNING); 	
		return false;
	}

	return true;
}

bool Renderer2D::Render(LineClass* p_RenderLine)
{
	// Render Model
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(LineVertexType); 

	// Set the drawing offset to zero (start drawing from the beginning)
	offset = 0;

	ID3D11Buffer *l_VertexBuffer = p_RenderLine->GetVertexBuffer();
	ID3D11Buffer *l_IndexBuffer = p_RenderLine->GetIndexBuffer();
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	m_DirectXDeviceContext->IASetVertexBuffers(0, 1, &l_VertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	m_DirectXDeviceContext->IASetIndexBuffer(l_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	m_DirectXDeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINESTRIP);

	// Render Shader
	if(!m_ColourShader->Render(m_DirectXDeviceContext, p_RenderLine->GetIndexCount(), m_WorldMatrix, m_ViewMatrix, m_OrthographicMatrix)) {
		MessageBox(NULL, L"The Colour Shader did not render correctly.", L"Error - Renderer2D::Render(LineClass*)", MB_OK | MB_ICONWARNING); 	
		return false;
	}

	return true;
}

void Renderer2D::Shutdown()
{
	// If this pointer exists then simply remove the pointer
	// Do not delete the object as it was not created in this class
	if(m_DirectXDeviceContext) {
		m_DirectXDeviceContext = NULL;
	}
}