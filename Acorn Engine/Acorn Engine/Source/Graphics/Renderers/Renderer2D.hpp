// 03/02/2014

#ifndef _RENDERER_2D_HPP_
#define _RENDERER_2D_HPP_

#include <windows.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <D3DX10math.h>

#include "../Shader/TextureShaderClass.hpp"
#include "../Objects/BitmapClass.hpp"
#include "../Objects/LineClass.hpp"
#include "../Shader/ColourShaderClass.hpp"

class Renderer2D
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};

	struct LineVertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR4 color;
	};

public:

	Renderer2D(void);

	~Renderer2D(void);

	bool Initialise(ID3D11DeviceContext*, D3DXMATRIX&, D3DXMATRIX&, D3DXMATRIX&, D3DXMATRIX&, TextureShaderClass*, ColourShaderClass*);

	bool Render(BitmapClass*);

	bool Render(LineClass*);

	void Shutdown();

private:

	ID3D11DeviceContext *m_DirectXDeviceContext;
	D3DXMATRIX			m_WorldMatrix;
	D3DXMATRIX			m_ProjectionMatrix;
	D3DXMATRIX			m_OrthographicMatrix;
	D3DXMATRIX			m_ViewMatrix;

	TextureShaderClass *m_TextureShader;
	ColourShaderClass *m_ColourShader;
};

#endif

