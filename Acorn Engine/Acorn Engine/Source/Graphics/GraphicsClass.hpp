// 02/02/2014

#ifndef _GRAPHICS_CLASS_HPP_
#define _GRAPHICS_CLASS_HPP_

#include "DirectX\DirectXClass.hpp"
#include "Shader\TextureShaderClass.hpp"
#include "Shader\ColourShaderClass.hpp"
#include "Renderers\Renderer2D.hpp"
#include "CameraClass.hpp"

class GraphicsClass
{
public:

	GraphicsClass(void);

	~GraphicsClass(void);

	bool Initialise(HWND window_handle, int screen_resolution_width, int screen_resolution_height);

	Renderer2D* Create2DRenderer();

	void Close2DRenderer(Renderer2D*);

	BitmapClass* CreateBitmap(int, int);

	void CloseBitmap(BitmapClass*);

	LineClass* CreateLine();

	void CloseLine(LineClass*);

	void Shutdown();

	void BeginScene(float, float, float, float);

	void EndScene();

private:

	DirectXClass *m_DirectXController;

	TextureShaderClass *m_TextureShader;

	ColourShaderClass *m_ColourShader;

	Renderer2D *m_Renderer2D;

	CameraClass *m_GameCamera;

	BitmapClass *m_Bitmap;

	LineClass *m_Line;

	int m_ScreenResolutionWidth, m_ScreenResolutionHeight;

};

#endif

