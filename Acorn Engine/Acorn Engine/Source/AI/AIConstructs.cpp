//////////////
// INCLUDES //
//////////////
#include "AIConstructs.hpp"

////////////////
// NAMESPACES //
////////////////
using namespace myai;

/////////////////////////////////////
// FUZZYVALUE FUNCTION DEFINITIONS //
/////////////////////////////////////
FuzzyValue::FuzzyValue()
{
	// Do nothing
}

FuzzyValue::~FuzzyValue()
{
	// Do nothing
}

void FuzzyValue::SetStart(double p_StartValue)
{
	// Set the start value to the passed value
	m_StartValue = p_StartValue;
	// State the end of the function
	return;
}

void FuzzyValue::SetEnd(double p_EndValue)
{
	// Set the end value to the passed value
	m_EndValue = p_EndValue;
	// State the end of the function
	return;
}

void FuzzyValue::SetFirstHigh(double p_FirstHighValue)
{
	// Set the first high value to the passed value
	m_FirstHighValue = p_FirstHighValue;
	// State the end of the function
	return;
}

void FuzzyValue::SetSecondHigh(double p_SecondHighValue)
{
	// Set the second high value to the passed value
	m_SecondHighValue = p_SecondHighValue;
	// State the end of the function
	return;
}

double FuzzyValue::GetStartValue()
{
	// Return the start value
	return m_StartValue;
}

double FuzzyValue::GetEndValue()
{
	// Return the end value
	return m_EndValue;
}

double FuzzyValue::GetFirstHighValue()
{
	// Return the first high value
	return m_FirstHighValue;
}

double FuzzyValue::GetSecondHighValue()
{
	// Return the second high value
	return m_SecondHighValue;
}

///////////////////////////////////
// FUZZYSET FUNCTION DEFINITIONS //
///////////////////////////////////
FuzzySet::FuzzySet()
{
	// Set the fuzzy value array pointer to null
	m_FuzzyValues = FUZZY_NULL;
}

FuzzySet::~FuzzySet()
{
	// Do nothing
}

bool FuzzySet::Initialise(int p_NumberOfFuzzyValues, double p_RangeStart, double p_RangeEnd)
{
	// If the number of fuzzy values in the set is meant to be zero or negative, return false
	// as such an occurance should never occur.
	if(p_NumberOfFuzzyValues <= 0) {
		return false;
	}

	// Set the number of fuzzy values in the set
	m_NumberOfFuzzyValues = p_NumberOfFuzzyValues;

	// Set the start range and end range values
	m_BeginRange = p_RangeStart;
	m_EndRange = p_RangeEnd;

	// Create the FuzzyValue array using the number of values in the set as passed in
	m_FuzzyValues = new FuzzyValue[m_NumberOfFuzzyValues];

	// Check the pointer has been corrected properly and return false if it has not
	if(!m_FuzzyValues) {
		return false;
	}

	// State the fuzzy set initilised properly
	return true;
}

void FuzzySet::SetFuzzyValue(double p_StartValue, double p_EndValue, double p_FirstHighValue, double p_SecondHighValue, int p_Number)
{
	// Set the values of the Fuzzy Value to the passed variables
	m_FuzzyValues[p_Number].SetStart(p_StartValue);
	m_FuzzyValues[p_Number].SetEnd(p_EndValue);
	m_FuzzyValues[p_Number].SetFirstHigh(p_FirstHighValue);
	m_FuzzyValues[p_Number].SetSecondHigh(p_SecondHighValue);
	// State the end of the function
	return;
}

FuzzyValue* FuzzySet::GetFuzzyValue(int p_Number)
{
	// Return the specified fuzzy value
	return &m_FuzzyValues[p_Number];
}

int FuzzySet::GetNumberOfFuzzyValues()
{
	// Return the number of fuzzy values
	return m_NumberOfFuzzyValues;
}

double FuzzySet::GetSetRangeBeginning()
{
	// Return the beginning range
	return m_BeginRange;
}

double FuzzySet::GetSetRangeEnd()
{
	// Return the end range
	return m_EndRange;
}

double FuzzySet::IsCrispWithinRange(const double p_CrispValue, const int p_Number)
{
	// Get the actual start and end values for the fuzzy value
	double l_ActualStartValue = GetActualFuzzyValue(m_FuzzyValues[p_Number].GetStartValue());
	double l_ActualEndValue = GetActualFuzzyValue(m_FuzzyValues[p_Number].GetEndValue());
	double l_ActualFirstHigh = GetActualFuzzyValue(m_FuzzyValues[p_Number].GetFirstHighValue());
	double l_ActualSecondHigh = GetActualFuzzyValue(m_FuzzyValues[p_Number].GetSecondHighValue());

	// See if the current fuzzy value has an infinity as one of its points
	if((m_FuzzyValues[p_Number].GetStartValue() == FUZZY_INFINITY) || (m_FuzzyValues[p_Number].GetEndValue() == FUZZY_INFINITY) || (m_FuzzyValues[p_Number].GetFirstHighValue() == FUZZY_INFINITY) || (m_FuzzyValues[p_Number].GetSecondHighValue() == FUZZY_INFINITY)) {
		// check if the crisp value is in the infinity line
		if(((m_FuzzyValues[p_Number].GetStartValue() == FUZZY_INFINITY) || (m_FuzzyValues[p_Number].GetFirstHighValue() == FUZZY_INFINITY)) && (p_CrispValue < l_ActualSecondHigh)) {
			return 1;
		}
		if(((m_FuzzyValues[p_Number].GetEndValue() == FUZZY_INFINITY) || (m_FuzzyValues[p_Number].GetSecondHighValue() == FUZZY_INFINITY)) && (p_CrispValue < l_ActualFirstHigh)) {
			return 1;
		}
		else {
			// Return the membership value with the specified fuzzy value
			return GetCrispMembershipValue(p_CrispValue, p_Number);
		}
	}
	

	// Check if the crisp value is within range. If it is the same value, still return zero as it
	// still have no membership
	if(((l_ActualStartValue > p_CrispValue) || (l_ActualStartValue == p_CrispValue)) || ((l_ActualEndValue < p_CrispValue) || (l_ActualEndValue == p_CrispValue))) {
		// Return zero to show there is no membership with the specified fuzzy value 
		return 0;
	}
	else {
		// Return the membership value with the specified fuzzy value
		return GetCrispMembershipValue(p_CrispValue, p_Number);
	}
	
}

void FuzzySet::Shutdown()
{
	// Check if the Fuzzy Value array exists
	if(m_FuzzyValues) {
		// Delete the pointer and then nullify it to be safe
		delete [] m_FuzzyValues;
		m_FuzzyValues = FUZZY_NULL;
	}

	// State the end of the function
	return;
}

double FuzzySet::GetCrispMembershipValue(const double p_CrispValue, const int p_Number)
{
	// Get the actual values for the begin, end, first high and second high in the Fuzzy Value
	double l_Begin = GetActualFuzzyValue(m_FuzzyValues[p_Number].GetStartValue());
	double l_End = GetActualFuzzyValue(m_FuzzyValues[p_Number].GetEndValue());
	double l_FirstHigh = GetActualFuzzyValue(m_FuzzyValues[p_Number].GetFirstHighValue());
	double l_SecondHigh = GetActualFuzzyValue(m_FuzzyValues[p_Number].GetSecondHighValue());
	
	// If the crisp value is between or equal to the first high and second high values then return 1.0
	// as it must have full membership.
	if((((p_CrispValue > l_FirstHigh) || (p_CrispValue == l_FirstHigh)) && ((p_CrispValue < l_SecondHigh) || (p_CrispValue == l_SecondHigh)))) {
		return 1;
	}

	// Check if it is between the begin or first high value - notice how the = sign is left out as
	// if it is equal to l_Begin or l_FirstHigh then it equals 0.0 or 1.0 respectively, which has all
	// ready been handled
	else if((p_CrispValue > l_Begin) && (p_CrispValue < l_FirstHigh)) {
		// Calculate the line between the two points - this is done by calculating the change in X (Y is 
		// 1.0 in all cases of this) and working out the gradiant.
		double l_ChangeInX = l_FirstHigh - l_Begin;
		double l_Gradiant = 1/l_ChangeInX;

		// The rest of the function being described as y = mx + c (where m is the gradiant and c is the intersection
		// point with the Y-axis) so by plugging in the correct variables we can get c. We will use the l_Begin value
		// as x and 0.0 as y. This means c is equal to negative mx
		double l_YIntersection = -(l_Gradiant*l_Begin);

		// We can now plug in the crisp value into this equation to get Y
		double l_Result = ((l_Gradiant*p_CrispValue) + l_YIntersection);

		// Return the result
		return l_Result;
	}

	// Check if it is between the second high or end value - notice how the = sign is left out again as
	// if it is equal to l_SecondHigh or l_End then it equals 1.0 or 0.0 respectively, which has all
	// ready been handled
	else if((p_CrispValue > l_SecondHigh) && (p_CrispValue < l_End)) {
		// Calculate the line between the two points - this is done by calculating the change in X (Y is 
		// -1.0 in all cases of this) and working out the gradiant.
		double l_ChangeInX = l_End - l_SecondHigh;
		double l_Gradiant = (-1)/l_ChangeInX;

		// The rest of the function being described as y = mx + c (where m is the gradiant and c is the intersection
		// point with the Y-axis) so by plugging in the correct variables we can get c. We will use the l_End value
		// as x and 0.0 as y. This means c is equal to negative mx
		double l_YIntersection = -(l_Gradiant*l_End);

		// We can now plug in the crisp value into this equation to get Y
		double l_Result = ((l_Gradiant*p_CrispValue) + l_YIntersection);

		// Return the result
		return l_Result;
	}

	// Should the above checks fail then something has gone wrong so return zero
	else {
		return 0;
	}
	
}

double FuzzySet::GetActualFuzzyValue(const double p_FuzzyValue)
{
	// Calculate the actual range
	double l_Result = m_EndRange - m_BeginRange;

	// Multiply the actual range by the fuzzy value
	l_Result = p_FuzzyValue*l_Result;

	// Add on the begin range to get it into the right place
	l_Result = l_Result + m_BeginRange;

	return l_Result;
}

////////////////////////////////////
// FUZZYRULE FUNCTION DEFINITIONS //
////////////////////////////////////
FuzzyRule::FuzzyRule()
{
	// Do nothing
}

FuzzyRule::~FuzzyRule()
{
	// Do nothing
}


double FuzzyRule::RunRule(double* p_Arguments, int p_Operation)
{
	// Create a local version of the result of the rule
	double l_Result = 0;

	// Go through the array of logical operators and select the correct one
	switch(p_Operation) {
		case FUZZY_MULTIPLY:
			// Perform operation
			l_Result = MultiplyOperation(p_Arguments);
			break;
		case FUZZY_DIVIDE:
			// Perform operation
			l_Result = DivisionOperation(p_Arguments);
			break;
		case FUZZY_ADD:
			// Perform operation
			l_Result = AdditionOperation(p_Arguments);
			break;
		case FUZZY_SUBTRACT:
			// Perform operation
			l_Result = SubtractionOperation(p_Arguments);
			break;
		case FUZZY_OR:
			// Perform operation
			l_Result = OROperation(p_Arguments);
			break;
		case FUZZY_AND:
			// Perform operation
			l_Result = ANDOperation(p_Arguments);
			break;
		case FUZZY_NOR:
			// Perform operation
			l_Result = NOROperation(p_Arguments);
			break;
		case FUZZY_NAND:
			// Perform operation
			l_Result = NANDOperation(p_Arguments);
			break;
		default:
			break;
	}

	// Return the result of the rule
	return l_Result;
}

double FuzzyRule::ANDOperation(double* p_Arguments)
{
	// Create a local version of the arguments - should be two so manually getting them is okay
	double l_ArgumentOne = p_Arguments[0];
	double l_ArgumentTwo = p_Arguments[1];

	// Multiply the two values together to get the AND value
	double l_Result = (l_ArgumentOne*l_ArgumentTwo);

	// Return the result
	return l_Result;
}

double FuzzyRule::OROperation(double* p_Arguments)
{
	// Create a local version of the arguments - should be two so manually getting them is okay
	double l_ArgumentOne = p_Arguments[0];
	double l_ArgumentTwo = p_Arguments[1];

	// Add the results then subtract the their product
	double l_Result = ((l_ArgumentOne + l_ArgumentTwo) - (l_ArgumentOne*l_ArgumentTwo));

	// Return the result
	return l_Result;
}

double FuzzyRule::NANDOperation(double* p_Arguments)
{
	// Create a local version of the arguments - should be two so manually getting them is okay
	double l_ArgumentOne = p_Arguments[0];
	double l_ArgumentTwo = p_Arguments[1];

	// Multiply the two values together to get the AND value
	double l_Result = (l_ArgumentOne*l_ArgumentTwo);

	// Return the opposite value by subtracting it from one
	return (1 - l_Result);
}

double FuzzyRule::NOROperation(double* p_Arguments)
{
	// Create a local version of the arguments - should be two so manually getting them is okay
	double l_ArgumentOne = p_Arguments[0];
	double l_ArgumentTwo = p_Arguments[1];

	// Add the results then subtract the their product
	double l_Result = ((l_ArgumentOne + l_ArgumentTwo) - (l_ArgumentOne*l_ArgumentTwo));

	// Return the opposite value by subtracting it from one
	return (1 - l_Result);
}

double FuzzyRule::MultiplyOperation(double* p_Arguments)
{
	// Create a local version of the arguments - should be two so manually getting them is okay
	double l_ArgumentOne = p_Arguments[0];
	double l_ArgumentTwo = p_Arguments[1];

	// Multiply the results
	double l_Result = (l_ArgumentOne*l_ArgumentTwo);

	// Return the result
	return l_Result;
}

double FuzzyRule::DivisionOperation(double* p_Arguments)
{
	// Create a local version of the arguments - should be two so manually getting them is okay
	double l_ArgumentOne = p_Arguments[0];
	double l_ArgumentTwo = p_Arguments[1];

	// Divide the first result by the second
	double l_Result = (l_ArgumentOne/l_ArgumentTwo);

	// Return the result
	return l_Result;
}

double FuzzyRule::SubtractionOperation(double* p_Arguments)
{
	// Create a local version of the arguments - should be two so manually getting them is okay
	double l_ArgumentOne = p_Arguments[0];
	double l_ArgumentTwo = p_Arguments[1];

	// Subtract the second result from the first
	double l_Result = (l_ArgumentOne - l_ArgumentTwo);

	// Return the result
	return l_Result;
}

double FuzzyRule::AdditionOperation(double* p_Arguments)
{
	// Create a local version of the arguments - should be two so manually getting them is okay
	double l_ArgumentOne = p_Arguments[0];
	double l_ArgumentTwo = p_Arguments[1];

	// Add the results
	double l_Result = (l_ArgumentOne + l_ArgumentTwo);

	// Return the result
	return l_Result;
}