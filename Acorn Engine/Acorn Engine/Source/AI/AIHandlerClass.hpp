// 13/02/2014

// This is the main handler class for the Fuzzy Logic AI system.

// The system has to be initialised which will create all the nessesary classes and
// initialise them properly so they will work with the application.

// The Run function takes two arguments, they are the input to the AI system so
// they are distance from the driving line and rate of change of distance from the
// driving line.
// The output is the value that will help determine how much the car has to rotate
// to follow the driving line.

// The shutdown function will close all related classes and zero any pointers created 
// in the class.

// This class adds a layer of abstarction for the Fuzzy AI. It's not nessesary but
// I think it helps keep the AI seperate from the main application.

#ifndef _AI_HANDLER_CLASS_HPP_
#define _AI_HANDLER_CLASS_HPP_

//////////////
// INCLUDES //
//////////////
#include "AIConstructs.hpp"
#include "FuzzyControlClass.hpp"

/////////////
// DEFINES //
/////////////
#define NUMBER_OF_RULE_RESULTS	7

// This is the namespace that all the AI functions are contained in
namespace myai {

	// This is the AI Handler Class. It is the only part the main program can
	// talk to, in order to access the Fuzzy Logic System.
	class AIHandlerClass
	{
		//////////////////////
		// PUBLIC FUNCTIONS //
		//////////////////////
		public:
			// Class Constructor
			AIHandlerClass(void);
			// Class Deconstructor
			~AIHandlerClass(void);
			// Initialise function to ensure all subsequent classes in this class create properly
			// and all variables are set to the correct value
			bool Initialise(void);
			// The Run function take the two inputs and process the data using Fuzzy Logic. It then
			// gives the application a value for how much the car should steer either left or right
			double Run(const double p_DistanceFromLine, const double p_AccelerationFromLine);
			// Closes all the handles and pointers in the class and ensures it is safe for the class
			// to shut down (as there is no automatic garbage collection).
			void Shutdown();

		///////////////////////
		// PRIVATE FUNCTIONS //
		///////////////////////
		private:
			// Sets up the fuzzy sets and gives them all the data they need for the fuzzy values
			bool SetUpFuzzySets(void);

		///////////////////////
		// PRIVATE VARIABLES //
		///////////////////////
		private:
			// A pointer to a FuzzyControlClass - This is the main part of the 
			// Fuzzy AI
			FuzzyControlClass *m_MainFuzzy;
			// Pointer to the distance fuzzy set
			FuzzySet *m_DistanceSet;
			// Pointer to the change in distance set
			FuzzySet *m_AccelerationSet;
			// Pinter to the defuzzification set
			FuzzySet *m_DefuzzificationSet;
			// Pointer to Fuzzy Rule
			FuzzyRule *m_TestRule;
	};
}

#endif

