//////////////
// INCLUDES //
//////////////
#include "AIHandlerClass.hpp"

////////////////
// NAMESPACES //
////////////////

// The myai namespace is used as this is the main code file for the AI Handler
// Class so rather than denoting everything as myai::, it is easier to simply
// include the namespace
using namespace myai;

//////////////////////////
// FUNCTION DEFINITIONS //
//////////////////////////

// AIClassHandler Constructor
AIHandlerClass::AIHandlerClass(void)
{
	// Zero any pointers here
	m_MainFuzzy = FUZZY_NULL;
	m_DistanceSet = FUZZY_NULL;
	m_AccelerationSet = FUZZY_NULL;
	m_DefuzzificationSet = FUZZY_NULL;
	m_TestRule = FUZZY_NULL;
}

// AIHandlerClass Deconstructor
AIHandlerClass::~AIHandlerClass(void)
{
	// Nothing to clean up here - mainly handled by the shutdown function
}

// AIHandlerClass Initialise Function - Creates any classes and initialises any
// variables in the class

// This works by passing the data to another class which will handle the conversion
// to fuzzy logic. This new class will return the value of 'how much it should turn'
// and this class converts the fuzzy logic to crisp logic
bool AIHandlerClass::Initialise(void)
{
	// Create a new FuzzyControlClass
	m_MainFuzzy = new FuzzyControlClass();
	// If the creation fails then end the function
	if(!m_MainFuzzy) {
		return false;
	}

	// Create the distance and acceleration set
	m_DistanceSet = m_MainFuzzy->CreateFuzzySet();
	m_AccelerationSet = m_MainFuzzy->CreateFuzzySet();
	m_DefuzzificationSet = m_MainFuzzy->CreateFuzzySet();

	// Check the sets were created correctly and return false if they were not
	if(!m_DistanceSet) {
		return false;
	}
	if(!m_AccelerationSet) {
		return false;
	}
	if(!m_DefuzzificationSet) {
		return false;
	}

	// Set up the fuzzy sets
	if(!SetUpFuzzySets()) {
		return false;
	}

	m_TestRule = new FuzzyRule();
	if(!m_TestRule) {
		return false;
	}

	// State that the initialisation of the AIHandlerClass was successful
	return true;
}

bool AIHandlerClass::SetUpFuzzySets(void)
{
	// Initialise the fuzzy sets with one fuzzy value each with a range between 0 and 100
	if(!m_DistanceSet->Initialise(1, -100, 100)) {
		return false;
	}

	if(!m_AccelerationSet->Initialise(1, -100, 100)) {
		return false;
	}

	if(!m_DefuzzificationSet->Initialise(1, 0, 1)) {
		return false;
	}

	// Create the fuzzy values in each set
	m_DistanceSet->SetFuzzyValue(FUZZY_INFINITY, 0.75, 0.4, 0.6, 0);
	m_AccelerationSet->SetFuzzyValue(0.4, 0.6, 0.45, 0.5, 0);
	m_DefuzzificationSet->SetFuzzyValue(0.1, 0.7, 0.3, 0.3, 0);

	// State the setting up of the fuzzy sets was successful
	return true;
}

// AIHandlerClass Run Function - Takes the two inputs and performs Fuzzy Logic
// on it to create an output.
double AIHandlerClass::Run(const double p_DistanceFromLine, const double p_AccelerationFromLine)
{
	// Create a float pointer array to hold the results from the fuzzy sets
	double *l_FuzzyDistanceResults = 0;
	double *l_FuzzyAccelerationResults = 0;

	// Run a crisp value through each fuzzy set 
	l_FuzzyDistanceResults = m_MainFuzzy->Run(m_DistanceSet, p_DistanceFromLine);
	l_FuzzyAccelerationResults = m_MainFuzzy->Run(m_AccelerationSet, p_AccelerationFromLine);

	// Create an argumnets array to pass into the rule and a results array to hold the results
	double *l_Arguments = new double[2];
	double *l_RuleResults = new double[NUMBER_OF_RULE_RESULTS];

	// Go through each expected result
	for(int i = 0; i < NUMBER_OF_RULE_RESULTS; i++) {
		// Go through this switch statement to see what to do for each rule
		switch(i) {
			case 0:
			{
				// Set the arguments 
				l_Arguments[0] = l_FuzzyDistanceResults[i];
				l_Arguments[1] = l_FuzzyAccelerationResults[i];
				// Process the rule
				l_RuleResults[i] = m_TestRule->RunRule(l_Arguments, FUZZY_AND);
				break;
			}
			case 1:
			{
				break;
			}
			case 2:
			{
				break;
			}
			case 3:
			{
				break;
			}
			case 4:
			{
				break;
			}
			case 5:
			{
				break;
			}
			case 6:
			{
				break;
			}
			default:
			{
				break;
			}
		}
	}

	// Defuzzification done to results
	double test = m_MainFuzzy->Defuzzification(l_RuleResults, 1, m_DefuzzificationSet);
	// Return final value

	// filler
	return 0.0f;
}

// AIHandlerClass Shutdown Function - This safely closes any classes and pointers
// created by this class and zero's any variables that need to be zeroed.
void AIHandlerClass::Shutdown(void) 
{
	// Ask the Fuzzy Control Class to close the fuzzy sets and rules
	if(m_DistanceSet) {
		m_DistanceSet->Shutdown();
		delete m_DistanceSet;
		m_DistanceSet = FUZZY_NULL;
	}

	if(m_AccelerationSet) {
		m_AccelerationSet->Shutdown();
		delete m_AccelerationSet;
		m_AccelerationSet = FUZZY_NULL;
	}

	if(m_DefuzzificationSet) {
		m_DefuzzificationSet->Shutdown();
		delete m_DefuzzificationSet;
		m_DefuzzificationSet = FUZZY_NULL;
	}

	if(m_TestRule) {
		delete m_TestRule;
		m_TestRule = FUZZY_NULL;
	}

	// If this FuzzyControlClass exists then call the shutdown function for that
	// class, delete the object, the nullify the pointer.
	if(m_MainFuzzy) {
		delete m_MainFuzzy;
		m_MainFuzzy = FUZZY_NULL;
	}

	// Despite returning nothing this denotes the end of the function.
	return;
}
