// 02/02/2014
/*
 * This is the class that handles the main run executions of the application.
 * It starts off by creating the window and storing essential data such as the window handle etc.
 * It loads in data from a file if specified to and if the file exists.
 * It then enters the main while loop of the game where it updates the game time and processes a single frame.
 * This class also contains the windows message loop for the program.
 */

#ifndef _SYSTEM_CLASS_HPP_
#define _SYSTEM_CLASS_HPP_

#include <Windows.h>

#include "Input\InputHandler.hpp"
#include "Timer\GameTimer.hpp"
#include "File Loader\TextFileReader.hpp"
#include "File Loader\WindowSettings.hpp"
#include "ProgramClass.hpp"
#include <string>

/////////////
// DEFINES //
/////////////
#define SCREEN_DEPTH 1000.0f
#define SCREEN_NEAR 0.1f
#define VSYNC_ENABLED true

class SystemClass
{
public:

	// Constructor
	SystemClass();

	// Deconstructor
	~SystemClass();

	bool Initialise(int mCmdLine, HINSTANCE hinstance);

	void Run();

	void Shutdown();

	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

private:

	bool CreateGameWindow();

	bool ParseWindowData();

private:

	HINSTANCE m_HInstance;

	LPCWSTR m_ApplicationName;

	HWND m_Hwnd;

	int m_AppScreenHeight, m_AppScreenWidth;

	bool m_AppRunning;

	InputHandler *m_Input;

	ProgramClass *m_Program;

	GameTimer *m_Timer;

	TextFileReader *m_SettingsReader;

	READTEXTDATA m_SettingsData;

	WindowSettings *m_WindowData;
};

/////////////////////////
// FUNCTION PROTOTYPES //
/////////////////////////
static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


/////////////
// GLOBALS //
/////////////
static SystemClass* ApplicationHandle = 0;

#endif

