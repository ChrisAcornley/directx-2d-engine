#include "ProgramClass.hpp"


ProgramClass::ProgramClass(void)
{
	// Zero any pointers
	m_FPS = 0;
	m_WindowHandle = 0;
	m_Input = 0;
	m_GraphicsManager = 0;
	m_Renderer2D = 0;
	m_Scene = 0;
}


ProgramClass::~ProgramClass(void)
{
	// Nothing to clean up
}

bool ProgramClass::Initialise(InputHandler* inputPointer, HWND hwnd, int p_ScreenRezWidth, int p_ScreenRezHeight)
{
	// Set the value to zero for first frame
	m_DeltaCount = 0.0f;

	// Pass the windows handle to the class instance
	m_WindowHandle = hwnd;

	// Set the input pointer to the passed instance
	m_Input = inputPointer;
	if(!m_Input) {
		return false;
	}

	// Create the new FPS object and check it was successful
	m_FPS = new FPSObject();
	if(!m_FPS) {
		return false;
	}

	// Create the Graphics Manager Object
	m_GraphicsManager = new GraphicsClass();
	if(!m_GraphicsManager) {
		return false;
	}

	// Initialise the graphics manager
	if(!m_GraphicsManager->Initialise(m_WindowHandle, p_ScreenRezWidth, p_ScreenRezHeight)) {
		return false;
	}

	// Create the 2D Renderer
	m_Renderer2D = m_GraphicsManager->Create2DRenderer();
	if(!m_Renderer2D) {
		return false;
	}

	// Create the scene 
	m_Scene = new SceneClass();
	if(!m_Scene) {
		return false;
	}

	// Initialise the scene
	if(!m_Scene->Initialise(m_GraphicsManager)) {
		return false;
	}

	// Initialisation was successful
	return true;
}

bool ProgramClass::ProcessGameFrame(float deltaTime)
{
	// Add the delta time onto the delta count
	m_DeltaCount += deltaTime;

	// See if the elapsed time since the last frame has been 16.66666 milliseconds (1/60th of a second)
	// This method means that the program will wait if the game is running too fast, and will allow the game to render scenes slower if needs be
	if(m_DeltaCount >= 16.6f) {

		// Update the Frame rate
		m_FPS->Frame(m_DeltaCount); 

		// Handle input in the main game loop
		HandleInput();

		// Perform update executions
		if(!m_Scene->Update(m_DeltaCount)) {
			return false;
		}

		// Render the Scene both graphics and audio
		m_GraphicsManager->BeginScene(0.0f, 0.0f, 1.0f, 1.0f);

		if(!m_Scene->Render(m_Renderer2D)) {
			return false;
		}

		m_GraphicsManager->EndScene();
		// Reset the counter for next frame
		m_DeltaCount = 0;
	}

	return true;
}

void ProgramClass::Shutdown()
{
	// Close the FPS class
	if(m_FPS) {
		delete m_FPS;
		m_FPS = 0;
	}

	// Close the input pointer
	if(m_Input) {
		m_Input = 0;
	}

	// Close the scene object
	if(m_Scene) {
		m_Scene->Shutdown(m_GraphicsManager);
		delete m_Scene;
		m_Scene = NULL;
	}

	// Close the renderer
	if(m_Renderer2D) {
		m_GraphicsManager->Close2DRenderer(m_Renderer2D);
		m_Renderer2D = 0;	
	}

	// Close the graphics manager
	if(m_GraphicsManager) {
		m_GraphicsManager->Shutdown();
		delete m_GraphicsManager;
		m_GraphicsManager = 0;
	}
}

void ProgramClass::HandleInput() {
	// Add input interaction that works for the entire program
}
