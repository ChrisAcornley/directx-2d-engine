#include "InputHandler.hpp"


InputHandler::InputHandler(void)
{
	m_MouseScaleX = 1.0f;
	m_MouseScaleY = 1.0f;
}

InputHandler::~InputHandler(void)
{
}

bool InputHandler::Initialise(int screenRezX, int screenRezY, int screenSizeX, int screenSizeY)
{
	for(int i = 0; i < 256; i++) {
		m_Keys[i] = false;
	}

	m_WheelValue = 0;

	m_LeftMouseDown = false;
	m_RightMouseDown = false;

	m_MouseScaleX = (float)screenRezX/(float)screenSizeX;
	m_MouseScaleY = (float)screenRezY/(float)screenSizeY;

	return true;
}

bool InputHandler::IsKeyDown(int keyID)
{
	return m_Keys[keyID];
}

void InputHandler::SetKeyDown(int keyID)
{
	m_Keys[keyID] = true;
}

void InputHandler::SetKeyUp(int keyID)
{
	m_Keys[keyID] = false;
}

void InputHandler::SetScrollWheelValue(int wheelScroll)
{
	m_WheelValue = wheelScroll;
}

int InputHandler::GetScrollWheelValue()
{
	return m_WheelValue;
}

void InputHandler::SetLeftMouseDown()
{
	m_LeftMouseDown = true;
}

void InputHandler::SetRightMouseDown()
{
	m_RightMouseDown = true;
}

void InputHandler::ReleaseLeftMouse()
{
	m_LeftMouseDown = false;
}

void InputHandler::ReleaseRightMouse()
{
	m_RightMouseDown = false;
}

bool InputHandler::IsLeftMouseDown()
{
	return m_LeftMouseDown;
}

bool InputHandler::IsRightMouseDown()
{
	return m_RightMouseDown;
}

int InputHandler::GetMouseXPosition()
{
	return (int)(m_MousePosition.x * m_MouseScaleX);
}

int InputHandler::GetMouseYPosition()
{
	return (int)(m_MousePosition.y * m_MouseScaleY);
}

void InputHandler::SetMousePosition()
{
	GetCursorPos(&m_MousePosition);
}
