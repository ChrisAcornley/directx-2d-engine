// 02/02/2014

#ifndef _INPUT_HANDLER_HPP_
#define _INPUT_HANDLER_HPP_

#include <Windows.h>

class InputHandler
{
public:

	InputHandler(void);

	~InputHandler(void);

	bool Initialise(int, int, int, int);

	bool IsKeyDown(int keyID);

	void SetKeyDown(int keyID);

	void SetKeyUp(int keyID);

	void SetScrollWheelValue(int wheelScroll);

	int GetScrollWheelValue();

	void SetLeftMouseDown();

	void SetRightMouseDown();

	void ReleaseLeftMouse();

	void ReleaseRightMouse();

	bool IsLeftMouseDown();

	bool IsRightMouseDown();

	int GetMouseXPosition();

	int GetMouseYPosition();

	void SetMousePosition();

	float GetMouseXScale() { return m_MouseScaleX; }

	float GetMouseYScale() { return m_MouseScaleY; }

private:

	bool m_Keys[256];

	int m_WheelValue;

	bool m_LeftMouseDown, m_RightMouseDown;

	POINT	m_MousePosition;

	float m_MouseScaleX;

	float m_MouseScaleY;
};

#endif


