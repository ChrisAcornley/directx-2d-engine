// 02/02/2014

#ifndef _FPSCLASS_H_
#define _FPSCLASS_H_

struct FPSObject
{
	int m_FPS;

	FPSObject() { m_FPS = 0; }

	void Frame(float deltaTime) { m_FPS = (int)(1000.0f/deltaTime); }

	int GetFrameRate() { return m_FPS; }
};

#endif

