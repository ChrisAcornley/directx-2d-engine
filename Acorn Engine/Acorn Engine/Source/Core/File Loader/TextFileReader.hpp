// 02/02/2014

// This class opens a text file and reads the data
// The data is passed back through the READTEXTDATA struct reference passed into the function

#ifndef _TEXT_FILE_READER_HPP_
#define _TEXT_FILE_READER_HPP_

#include <fstream>
#include <string>
using namespace std;

#define MAXIMUM_TEXT_LINES 256

struct READTEXTDATA
{
	string m_ReadText[MAXIMUM_TEXT_LINES];
	int m_NumberOfLines;
};

class TextFileReader
{
public:

	TextFileReader(void);

	~TextFileReader(void);

	bool ReadFile(string, READTEXTDATA&);

private:

	ifstream m_DataStream;

	int m_CurrentLine;
};

#endif

