#include "TextFileReader.hpp"


TextFileReader::TextFileReader(void)
{
}

TextFileReader::~TextFileReader(void)
{
}

bool TextFileReader::ReadFile(string p_Filename, READTEXTDATA &p_DataHandle)
{
	// Ensure the current line being written to is the first in the string array
	m_CurrentLine = 0;

	// Open the input stream using the filename
	m_DataStream.open(p_Filename);
	// Check if the data stream fails to open the file
	if(!m_DataStream.is_open()) {
		return false;
	}

	// Run this loop while the stream is not at the end of the file
	while(!m_DataStream.eof())
	{
		// Pass the current line from the data stream into the string array using m_CurrentLine as the index
		getline(m_DataStream, p_DataHandle.m_ReadText[m_CurrentLine]);

		// Increment the index to move to the next line
		m_CurrentLine++;
	}

	// Set the number of lines filled
	p_DataHandle.m_NumberOfLines = m_CurrentLine;

	// Close the data stream as the data has now been read
	m_DataStream.close();

	// Return that the file reading has succeeded
	return true;
}
