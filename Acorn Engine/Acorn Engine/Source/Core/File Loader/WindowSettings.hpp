// 02/02/2014

// Got the code for the initialisation list from
// http://en.cppreference.com/w/cpp/language/initializer_list
// 15:12 - 02/02/2014

#ifndef _WINDOW_SETTINGS_HPP_
#define _WINDOW_SETTINGS_HPP_

#include <string>

struct WindowSettings {
	int m_WindowHeight, m_WindowWidth;
	int m_WindowResolutionWidth, m_WindowResolutionHeight;
	bool m_FullScreen;
	bool m_Windowed;
	int m_WindowPositionX, m_WindowPositionY;
	string m_ApplicationName;

	WindowSettings()	: m_WindowHeight(600) ,
						  m_WindowWidth(800) ,
						  m_WindowResolutionWidth(1280) ,
						  m_WindowResolutionHeight(720) , 
						  m_FullScreen(false) ,
						  m_Windowed(true) ,
						  m_WindowPositionX (0) ,
						  m_WindowPositionY(0) ,
						  m_ApplicationName("No Name")
	{
		// Set the defualt window dimensions to 800x600.
		// Set the window resolution to 1280x720 (720p or 16:9)
		// Set the window to not be full screen
		// State that the window is windowed (can see border)
		// Set the windows position at (0,0) or the top left hand corner of the screen
		// Set the window name to "no name"
		// Got this data from (http://en.wikipedia.org/wiki/List_of_common_resolutions) on 02/02/2014 at 14:42pm
	}
};

#endif