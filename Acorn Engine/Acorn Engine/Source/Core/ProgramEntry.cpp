// 02/02/2013
// Uses code from WindowsBase project I created in 2013
// This project also uses code I submitted for my Advanced Games Technologies module
// So this is a refernece note of sorts to say I am using my own code.

#include <Windows.h>

#include "SystemClass.hpp"

int WINAPI WinMain (HINSTANCE hinstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{
	// Create pointer to system class
	SystemClass *system;

	// Create instance of SystemClass
	system = new SystemClass();
	// If the pointer is still empty - close the program
	if(!system) {
		return 0;
	}

	// Initialise the system class and pass in showCmd and hinstance - if it fails close the program
	if(system->Initialise(showCmd, hinstance)) {
		// Run the programs main loop
		system->Run();
	}

	
	// If the system object exists
	if(system) {
		// Call the shutdown of the system
		system->Shutdown();
		// Delete the system object
		delete system;
		// Zero the pointer to the system object
		system = 0;
	}

	// End program
	return 0;
}