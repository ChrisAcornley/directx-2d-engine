#include "SystemClass.hpp"

// Constructor
SystemClass::SystemClass()
{
	m_Timer = 0;
	m_Program = 0;
	m_Input = 0;
	m_SettingsReader = 0;
	m_WindowData = 0;
}

// Deconstructor
SystemClass::~SystemClass()
{
}

bool SystemClass::Initialise(int mCmdLine, HINSTANCE hinstance)
{
	// Set the class instance to the passed instance
	m_HInstance = hinstance;

	// Set the pointer to this class
	ApplicationHandle = this;

	// Initialise the window settings data struct
	m_WindowData = new WindowSettings();
	if(!m_WindowData) {
		// Create a message box to display error - note it is not bound to the window as it has not been created yet
		MessageBox(NULL, L"The WindowSettings class failed to initialise.", L"Error - WindowSettings()", MB_OK | MB_ICONWARNING);
		return false;
	}

	// Try to open the settings file and read in the data
	m_SettingsReader = new TextFileReader();
	if(!m_SettingsReader) {
		// Create a message box to display error - note it is not bound to the window as it has not been created yet
		MessageBox(NULL, L"The text reader failed to initialise.", L"Error - TextFileReader()", MB_OK | MB_ICONWARNING);
		return false;
	}

	// Try and read the data from the settings file
	if(!m_SettingsReader->ReadFile("Resources/WindowSettings/WindowSettings.txt", m_SettingsData)) {
		// Create a message box to display error - note it is not bound to the window as it has not been created yet
		MessageBox(NULL, L"Failed to read the WindowSettings.txt file. Using default data.", L"Error - Couldn't Read File", MB_OK | MB_ICONWARNING);
	}
	// If the file reads correctly then parse the data
	else {
		// Parse the data and check it was successful
		if(!ParseWindowData()) {
			// Create a message box to display error - note it is not bound to the window as it has not been created yet
			MessageBox(NULL, L"Failed to parse and load in data.", L"Error - Couldn't Parse Data", MB_OK | MB_ICONWARNING);
		}
	}

	// Create Window
	if(!CreateGameWindow()) {
		return false;
	}

	// Create an instance of the input handler
	m_Input = new InputHandler();
	if(!m_Input) {
		// Create a windows message box and state what happened
		MessageBox(m_Hwnd, L"The input handler has failed to start.", L"Error - InputHandler()", MB_OK | MB_ICONWARNING);
		return false;
	}

	if(!m_Input->Initialise(m_WindowData->m_WindowResolutionWidth, m_WindowData->m_WindowResolutionHeight, m_WindowData->m_WindowWidth, m_WindowData->m_WindowHeight)) {
		// Create a windows message box and state what happened
		MessageBox(m_Hwnd, L"The input handler has failed to initialise.", L"Error - Initialise()", MB_OK | MB_ICONWARNING);
		return false;
	}

	// Create a instance of the program class
	m_Program = new ProgramClass();
	if(!m_Program) {
		// Create a windows message box and state what happened
		MessageBox(m_Hwnd, L"The program class has failed to start.", L"Error - ProgramClass()", MB_OK | MB_ICONWARNING);
		return false;
	}

	if(!m_Program->Initialise(m_Input, m_Hwnd, m_WindowData->m_WindowResolutionWidth, m_WindowData->m_WindowResolutionHeight)) {
		// Create a windows message box and state what happened
		MessageBox(m_Hwnd, L"The program class has failed to initialise.", L"Error - Initialise()", MB_OK | MB_ICONWARNING);
		return false;
	}

	// Create a new instance of the game timer
	m_Timer = new GameTimer();
	if(!m_Timer) {
		// Create a windows message box and state what happened
		MessageBox(m_Hwnd, L"The game timer has failed to start.", L"Error - GameTimer()", MB_OK | MB_ICONWARNING);
		return false;
	}

	// Reset the timer object
	m_Timer->Reset();

	return true;
}

void SystemClass::Run()
{
	MSG msg;

	// Initialize the message structure.
	ZeroMemory(&msg, sizeof(MSG));
	
	// Loop until there is a quit message from the window or the user.
	m_AppRunning = true;
	while(m_AppRunning)
	{
		// Handle the windows messages.
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// If windows signals to end the application then exit out.
		if(msg.message == WM_QUIT)
		{
			m_AppRunning = false;
		}
		else
		{
			// Tick the timer
			m_Timer->Tick();

			// If the Escape key is down
			if(m_Input->IsKeyDown(VK_ESCAPE)) {
				// Close the program
				m_AppRunning = false;
			}

			// Try and successfully process game frame
			if(!m_Program->ProcessGameFrame(m_Timer->GetDeltaTimeMS())) {
				// If this fails end the application
				m_AppRunning = false;
			}
		}
	}
}

void SystemClass::Shutdown()
{
	// If Program object exists
	if(m_Program) {
		// Call for class shutdown, delete the object and zero the pointer
		m_Program->Shutdown();
		delete m_Program;
		m_Program = 0;
	}

	// If Program object exists
	if(m_Timer) {
		// Delete the object and zero the pointer
		delete m_Timer;
		m_Timer = 0;
	}

	// If Program object exists
	if(m_Input) {
		// Delete the object and zero the pointer
		delete m_Input;
		m_Input = 0;
	}

	// Show the mouse cursor.
	ShowCursor(true);

	// Fix the display settings if leaving full screen mode.
	if(m_WindowData->m_FullScreen)
	{
		ChangeDisplaySettings(NULL, 0);
	}

	// Remove the window.
	DestroyWindow(m_Hwnd);
	m_Hwnd = NULL;

	// Remove the application instance.
	UnregisterClass(m_ApplicationName, m_HInstance);
	m_HInstance = NULL;

	// Release the pointer to this class.
	ApplicationHandle = NULL;
}

LRESULT CALLBACK SystemClass::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	// Create a temorary RECT object pointer
	RECT *windowRectangle = new RECT();

	m_Input->SetMousePosition();

	switch(umsg)
	{
		case WM_KEYDOWN:
			// If a key is pressed, set it to down
			m_Input->SetKeyDown(wparam);
			break;

		case WM_KEYUP:
			// If the key is released, set it to up
			m_Input->SetKeyUp(wparam);
			break;

		case WM_LBUTTONDOWN:
			// If the left mouse button is clicked, set it to clicked
			m_Input->SetLeftMouseDown();
			break;

		case WM_LBUTTONUP:
			// If the left mouse button is released, set it to released
			m_Input->ReleaseLeftMouse();
			break;

		case WM_RBUTTONDOWN:
			// If the right mouse button is clicked, set it to clicked
			m_Input->SetRightMouseDown();
			break;

		case WM_RBUTTONUP:
			// If the left mouse button is released, set it to released
			m_Input->ReleaseLeftMouse();
			break;

		case WM_MOUSEWHEEL:
			// If the wheel is scrolled, set the value
			m_Input->SetScrollWheelValue(wparam);
			break;

		case WM_MOVING:
			//if(m_Settings.FULL_SCREEN) {
				// Set the RECT pointer to be equal to the window rectangle object
			//	windowRectangle = (RECT*)lparam;
				// Set the values of the rectangle so it fills the whole screen (this prevents the screen from moving)
			//	windowRectangle->left = 0;
			//	windowRectangle->top = 0;
			//	windowRectangle->right = m_AppScreenWidth;
			//	windowRectangle->bottom = m_AppScreenHeight;
			//}
			break;
			
		default:
			// Do nothing for these
			break;

	}
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}

bool SystemClass::CreateGameWindow()
{
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;
	wstring l_AppName;
	DWORD windowOutline;

	// Creates a temporary string object and passes it that data from the settings struct
	l_AppName = wstring(m_WindowData->m_ApplicationName.begin(), m_WindowData->m_ApplicationName.end());

	// Creates the application name
	m_ApplicationName = l_AppName.c_str();

	// Setup the windows class with default settings.
	wc.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc   = WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = m_HInstance;
	wc.hIcon		 = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm       = wc.hIcon;
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = m_ApplicationName;
	wc.cbSize        = sizeof(WNDCLASSEX);

	// Register the class
	RegisterClassEx(&wc);

	// Set the window width and height to fill the entire screen
	m_AppScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	m_AppScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	if(!m_WindowData->m_Windowed)
	{
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize       = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth  = (unsigned long)m_AppScreenWidth;
		dmScreenSettings.dmPelsHeight = (unsigned long)m_AppScreenHeight;
		dmScreenSettings.dmBitsPerPel = 32;			
		dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = posY = 0;

		// Set the window style properties
		windowOutline = WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP;
	}
	else
	{
		if(!m_WindowData->m_FullScreen) {
			// If windowed then set it to the values set in the windows setting class
			m_AppScreenWidth  = m_WindowData->m_WindowWidth;
			m_AppScreenHeight = m_WindowData->m_WindowHeight;

			// Place the window in the middle of the screen.
			posX = m_WindowData->m_WindowPositionX;
			posY = m_WindowData->m_WindowPositionY;
		}
		else {
			// Place the window in the top left corner
			posX = posY = 0;
		}

		// Set the window style properties
		windowOutline = WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_SYSMENU;
	}

	// Create the window with the screen settings and get the handle to it.
	m_Hwnd = CreateWindowEx(WS_EX_APPWINDOW, 
							m_ApplicationName, 
							m_ApplicationName, 
							windowOutline,
							posX, 
							posY, 
							m_AppScreenWidth, 
							m_AppScreenHeight, 
							NULL, 
							NULL, 
							m_HInstance, 
							NULL);
	
	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_Hwnd, SW_SHOW);
	SetForegroundWindow(m_Hwnd);
	SetFocus(m_Hwnd);

	// Hide the mouse cursor.
	//ShowCursor(false);

	return true;
}

bool SystemClass::ParseWindowData()
{
	// Create a local holder for the current string being read
	// pass it through a set of if checks to see what it is
	// set the right value

	string l_CurrentString;
	string l_ParseDelim = ": ";
	string l_FindString;
	int l_FindPos = 0;
	int l_CurrentLine = 0;
	bool l_CheckingStrings = true;

	while(l_CheckingStrings) {
		// Get the first string
		l_CurrentString = m_SettingsData.m_ReadText[l_CurrentLine];

		// Set the find string as "WindowSizeX"
		l_FindString = "WindowSizeX";
		// Try and find the 'find string' in the current string being analysed
		l_FindPos = l_CurrentString.find(l_FindString);
		// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
		// -1 is returned so this if statement won't find.
		if(l_FindPos == 0) {
			// Find the position of the proper delim ": "
			l_FindPos = l_CurrentString.find(l_ParseDelim);
			// Delete everything up to and including the delimc and an extra space (to include the space in the text)
			l_CurrentString.erase(0, l_FindPos + l_ParseDelim.length());
			// Covert the string into integer data
			m_WindowData->m_WindowWidth = atoi(l_CurrentString.c_str());
		}

		// Set the find string as "Application Name"
		l_FindString = "Application Name";
		// Try and find the 'find string' in the current string being analysed
		l_FindPos = l_CurrentString.find(l_FindString);
		// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
		// -1 is returned so this if statement won't find.
		if(l_FindPos == 0) {
			// Find the position of the proper delim ": "
			l_FindPos = l_CurrentString.find(l_ParseDelim);
			// Delete everything up to and including the delimc and an extra space (to include the space in the text)
			l_CurrentString.erase(0, l_FindPos + l_ParseDelim.length());
			// Pass in the application name
			m_WindowData->m_ApplicationName = l_CurrentString;
		}

		// Set the find string as "WindowSizeY"
		l_FindString = "WindowSizeY";
		// Try and find the 'find string' in the current string being analysed
		l_FindPos = l_CurrentString.find(l_FindString);
		// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
		// -1 is returned so this if statement won't find.
		if(l_FindPos == 0) {
			// Find the position of the proper delim ": "
			l_FindPos = l_CurrentString.find(l_ParseDelim);
			// Delete everything up to and including the delimc and an extra space (to include the space in the text)
			l_CurrentString.erase(0, l_FindPos + l_ParseDelim.length());
			// Covert the string into integer data
			m_WindowData->m_WindowHeight = atoi(l_CurrentString.c_str());
		}

		// Set the find string as "WindowResolutionX"
		l_FindString = "WindowResolutionX";
		// Try and find the 'find string' in the current string being analysed
		l_FindPos = l_CurrentString.find(l_FindString);
		// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
		// -1 is returned so this if statement won't find.
		if(l_FindPos == 0) {
			// Find the position of the proper delim ": "
			l_FindPos = l_CurrentString.find(l_ParseDelim);
			// Delete everything up to and including the delimc and an extra space (to include the space in the text)
			l_CurrentString.erase(0, l_FindPos + l_ParseDelim.length());
			// Covert the string into integer data
			m_WindowData->m_WindowResolutionWidth = atoi(l_CurrentString.c_str());
		}

		// Set the find string as "WindowResolutionY"
		l_FindString = "WindowResolutionY";
		// Try and find the 'find string' in the current string being analysed
		l_FindPos = l_CurrentString.find(l_FindString);
		// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
		// -1 is returned so this if statement won't find.
		if(l_FindPos == 0) {
			// Find the position of the proper delim ": "
			l_FindPos = l_CurrentString.find(l_ParseDelim);
			// Delete everything up to and including the delimc and an extra space (to include the space in the text)
			l_CurrentString.erase(0, l_FindPos + l_ParseDelim.length());
			// Covert the string into integer data
			m_WindowData->m_WindowResolutionHeight = atoi(l_CurrentString.c_str());
		}

		// Set the find string as "WindowX"
		l_FindString = "WindowX";
		// Try and find the 'find string' in the current string being analysed
		l_FindPos = l_CurrentString.find(l_FindString);
		// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
		// -1 is returned so this if statement won't find.
		if(l_FindPos == 0) {
			// Find the position of the proper delim ": "
			l_FindPos = l_CurrentString.find(l_ParseDelim);
			// Delete everything up to and including the delimc and an extra space (to include the space in the text)
			l_CurrentString.erase(0, l_FindPos + l_ParseDelim.length());
			// Covert the string into integer data
			m_WindowData->m_WindowPositionX = atoi(l_CurrentString.c_str());
		}

		// Set the find string as "WindowY"
		l_FindString = "WindowY";
		// Try and find the 'find string' in the current string being analysed
		l_FindPos = l_CurrentString.find(l_FindString);
		// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
		// -1 is returned so this if statement won't find.
		if(l_FindPos == 0) {
			// Find the position of the proper delim ": "
			l_FindPos = l_CurrentString.find(l_ParseDelim);
			// Delete everything up to and including the delimc and an extra space (to include the space in the text)
			l_CurrentString.erase(0, l_FindPos + l_ParseDelim.length());
			// Covert the string into integer data
			m_WindowData->m_WindowPositionY = atoi(l_CurrentString.c_str());
		}

		// Set the find string as "FullScreen"
		l_FindString = "FullScreen";
		// Try and find the 'find string' in the current string being analysed
		l_FindPos = l_CurrentString.find(l_FindString);
		// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
		// -1 is returned so this if statement won't find.
		if(l_FindPos == 0) {
			// Find the position of the proper delim ": "
			l_FindPos = l_CurrentString.find(l_ParseDelim);
			// Delete everything up to and including the delimc and an extra space (to include the space in the text)
			l_CurrentString.erase(0, l_FindPos + l_ParseDelim.length());
			// Covert the string into integer data - this is cast as a boolean
			m_WindowData->m_FullScreen = (bool)atoi(l_CurrentString.c_str());
		}

		// Set the find string as "Windowed"
		l_FindString = "Windowed";
		// Try and find the 'find string' in the current string being analysed
		l_FindPos = l_CurrentString.find(l_FindString);
		// If the string is found it will be at the front or at position 0. If it fails to find the 'find string' then
		// -1 is returned so this if statement won't find.
		if(l_FindPos == 0) {
			// Find the position of the proper delim ": "
			l_FindPos = l_CurrentString.find(l_ParseDelim);
			// Delete everything up to and including the delimc and an extra space (to include the space in the text)
			l_CurrentString.erase(0, l_FindPos + l_ParseDelim.length());
			// Covert the string into integer data
			m_WindowData->m_Windowed = atoi(l_CurrentString.c_str());
		}

		// Move to the next string
		l_CurrentLine++; 

		// Check if the loop has exceeded the number of lines currently filled in the array
		if(l_CurrentLine >= m_SettingsData.m_NumberOfLines) {
			l_CheckingStrings = false;
		}
	}

	return true;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	switch(umessage)
	{
		// Check if the window is being destroyed.
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}

		// Check if the window is being closed.
		case WM_CLOSE:
		{
			PostQuitMessage(0);		
			return 0;
		}

		// All other messages pass to the message handler in the system class.
		default:
		{
			return ApplicationHandle->MessageHandler(hwnd, umessage, wparam, lparam);
		}
	}
}