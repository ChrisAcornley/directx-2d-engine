#include "SceneClass.hpp"

SceneClass::SceneClass(void)
{
	m_CarObject = NULL;
	m_DrivingLine = NULL;
	m_AIManager = NULL;
}

SceneClass::~SceneClass(void)
{
	// Nothing to clean up
}

bool SceneClass::Initialise(GraphicsClass* p_GraphicsManager)
{
	// Initialise any objects here
	m_CarObject = p_GraphicsManager->CreateBitmap(100,100);
	if(!m_CarObject) {
		return false;
	}

	m_DrivingLine = p_GraphicsManager->CreateLine();
	if(!m_DrivingLine) {
		return false;
	}

	// Load textures in for the bitmaps
	if(!LoadBitmapTextures()) {
		return false;
	}

	// Create the AI manager
	m_AIManager = new myai::AIHandlerClass();
	if(!m_AIManager) {
		return false;
	}

	// Initialise the AI manager
	if(!m_AIManager->Initialise()) {
		return false;
	}

	// As the graphics class pointer is only active in this functions scope, nullify it to be sure there is not rogue
	// pointer floating about in space.
	p_GraphicsManager = NULL;

	return true;
}

bool SceneClass::Update(float p_DeltaTime)
{
	// Update any objects here
	if(!m_CarObject->Update(100,100)) {
		return false;
	}

	double test = m_AIManager->Run(21,-15);

	return true;
}

bool SceneClass::Render(Renderer2D* p_Renderer)
{
	// Render any objects here
	p_Renderer->Render(m_CarObject);
	p_Renderer->Render(m_DrivingLine);

	// Nullify the Renderer pointer as it loses scope once this function ends so this ensures it is not a random pointer
	// in the systems memory
	p_Renderer = NULL;

	return true;
}
	
void SceneClass::Shutdown(GraphicsClass* p_GraphicsManager)
{
	// Close any objects here
	if(m_CarObject) {
		p_GraphicsManager->CloseBitmap(m_CarObject);
		m_CarObject = NULL;
	}

	if(m_DrivingLine) {
		p_GraphicsManager->CloseLine(m_DrivingLine);
		m_DrivingLine = NULL;
	}

	// Close the AI manager
	if(m_AIManager) {
		m_AIManager->Shutdown();
		delete m_AIManager;
		m_AIManager = 0;
	}

	// As the graphics class pointer is only active in this functions scope, nullify it to be sure there is not rogue
	// pointer floating about in space.
	p_GraphicsManager = NULL;

	return;
}

bool SceneClass::LoadBitmapTextures()
{
	if(!m_CarObject->LoadTexture(L"Resources/Textures/seafloor.dds")) {
		return false;
	}

	return true;
}