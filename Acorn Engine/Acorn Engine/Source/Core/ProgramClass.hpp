// 02/02/2014

#ifndef _PROGRAMCLASS_HPP_s
#define _PROGRAMCLASS_HPP_

#include "FPSObject.hpp"
#include "SceneClass.hpp"
#include "Input/InputHandler.hpp"
#include "../Graphics/GraphicsClass.hpp"
#include "../Graphics/Objects/BitmapClass.hpp"

#include <Windows.h>

class ProgramClass
{
public:

	ProgramClass(void);

	~ProgramClass(void);

	bool Initialise(InputHandler* inputPointer, HWND hwnd, int p_ScreenRezWidth, int p_ScreenRezHeight);

	bool ProcessGameFrame(float deltaTime);

	void Shutdown();

private:

	void HandleInput();

private:

	float m_DeltaCount;

	HWND m_WindowHandle;

	FPSObject *m_FPS;

	InputHandler *m_Input;

	GraphicsClass *m_GraphicsManager;

	Renderer2D *m_Renderer2D;

	SceneClass *m_Scene;
};

#endif

