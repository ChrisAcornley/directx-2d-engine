// 10/02/2014

#ifndef _SCENE_CLASS_HPP_
#define _SCENE_CLASS_HPP_

#include "../Graphics/Renderers/Renderer2D.hpp"
#include "../Graphics/Objects/BitmapClass.hpp"
#include "../Graphics/Objects/LineClass.hpp"
#include "../Graphics/GraphicsClass.hpp"
#include "../AI/AIHandlerClass.hpp"

class SceneClass
{
public:

	SceneClass(void);

	~SceneClass(void);

	bool Initialise(GraphicsClass* p_GraphicsManager);

	bool Update(float p_DeltaTime);

	bool Render(Renderer2D* p_Renderer);
	
	void Shutdown(GraphicsClass*);

private:

	bool LoadBitmapTextures();

private:

	BitmapClass *m_CarObject;

	LineClass *m_DrivingLine;

	myai::AIHandlerClass *m_AIManager;
};

#endif
